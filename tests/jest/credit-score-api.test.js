let result = {};
let basePath = 'http://localhost:8080';
const request = require('request-promise');

describe('Request with DENIED result', () =>{
    beforeAll((callback) => {
        let options = {
            method: 'POST',
            url: basePath + '/creditscore/',
            headers: {
                'content-type': 'application/json'
            },
            body: {
                age: 20,
                homeOwnership: 'rent',
                income: 9999,
                loanAmount: 1000,
                type: 'personal'
            },
            json: true
        };

        request(options).then((res) => {
            Object.assign(result, res);
            callback();
        }).catch((error)=>{
            callback();
        });
    });

    test('Result should be DENIED', async () => {
        await expect(result.approval).toMatch(/DENIED/);
    });

    test('Credit Score should be 330', async () =>{
        await expect(result.creditScore).toEqual(330);
    });

    test('Credit Rating should be POOR', async ()=>{
        await expect(result.creditRating).toMatch(/Poor/);
    });

});

describe('Request with ACCEPTED result with GREAT score', () => {
    beforeAll((callback) => {
        let options = {
            method: 'POST',
            url: basePath + '/creditscore/',
            headers: {
                'content-type': 'application/json'
            },
            body: {
                age: 20,
                homeOwnership: 'own',
                income: 100000,
                loanAmount: 2000,
                type: 'personal'
            },
            json: true
        };

        request(options).then((res) => {
            Object.assign(result, res);
            callback();
        }).catch((error)=>{
            callback();
        });
    });

    test('Result should be ACCEPTED', async () => {
        await expect(result.approval).toMatch(/ACCEPTED/);
    });

    test('Credit score should be 550', async () => {
        await expect(result.creditScore).toEqual(550);
    });

    test('Good credit rating', async () => {
        await expect(result.creditRating).toMatch(/Good/);
    });

    test('Loan interest should be 18.9', async () => {
        await expect(result.interest).toEqual(18.9);
    });

    test('Loan Threshold should be 40000', async () => {
        await expect(result.loanThreshold).toEqual(40000);
    });

    test('Check for Calculations', async () => {
        await expect(result.calculation).toHaveProperty('threeYear');
        await expect(result.calculation).toHaveProperty('fiveYear');
    });

    test('threeYear approved loan of 2000',async () => {
        await expect(result.calculation.threeYear.approvedLoan).toEqual(2000);
    });

    test('threeYear monthly repayment of 87.05555555555556', async () => {
        await expect(result.calculation.threeYear.monthlyRepayment).toEqual(87.05555555555556);
    });

    test('threeYear total repayment 3134', async () => {
        await expect(result.calculation.threeYear.totalRepayment).toEqual(3134);
    });

    test('threeYear total interest 1134', async () => {
        await expect(result.calculation.threeYear.totalInterest).toEqual(1134);
    });

     test('fiveYear approved loan of 2000', async () => {
        await expect(result.calculation.fiveYear.approvedLoan).toEqual(2000);
    });

    test('fiveYear monthly repayment of 64.83333333333333', async () => {
        await expect(result.calculation.fiveYear.monthlyRepayment).toEqual(64.83333333333333);
    });

    test('fiveYear total repayment 3890', async () => {
        await expect(result.calculation.fiveYear.totalRepayment).toEqual(3890);
    });

    test('fiveYear total interest 1890', async () => {
        await expect(result.calculation.fiveYear.totalInterest).toEqual(1890);
    });
});

describe('Request for loan ACCEPTED and has a GOOD score' , () => {
    beforeAll((callback) => {
    let options = {
        method: 'POST',
        url: basePath + '/creditscore/',
        headers: {
            'content-type': 'application/json'
        },
        body: {
            age: 30,
            homeOwnership: 'own',
            income: 50000,
            loanAmount: 1000,
            type: 'business'
        },
        json: true
    };

        request(options).then((res) => {
            Object.assign(result, res);
            callback();
        }).catch((error)=>{
            callback();
        });
    });

    test('Result should be ACCEPTED', async () => {
        await expect(result.approval).toMatch(/ACCEPTED/);
    });

    test('Credit score should be 545', async () => {
        await expect(result.creditScore).toEqual(545);
    });

    test('Good credit rating', async () => {
        await expect(result.creditRating).toMatch(/Good/);
    });

    test('Loan interest should be 18.9', async () => {
        await expect(result.interest).toEqual(18.9);
    });

    test('Loan Threshold should be 40000', async () => {
        await expect(result.loanThreshold).toEqual(40000);
    });

    test('Check for Calculations', async () => {
        await expect(result.calculation).toHaveProperty('threeYear');
        await expect(result.calculation).toHaveProperty('fiveYear');
    });

    test('threeYear approved loan of 1000',async () => {
        await expect(result.calculation.threeYear.approvedLoan).toEqual(1000);
    });

    test('threeYear monthly repayment of 43.52777777777778', async () => {
        await expect(result.calculation.threeYear.monthlyRepayment).toEqual(43.52777777777778);
    });

    test('threeYear total repayment 1567', async () => {
        await expect(result.calculation.threeYear.totalRepayment).toEqual(1567);
    });

    test('threeYear total interest 567', async () => {
        await expect(result.calculation.threeYear.totalInterest).toEqual(567);
    });

     test('fiveYear approved loan of 1000', async () => {
        await expect(result.calculation.fiveYear.approvedLoan).toEqual(1000);
    });

    test('fiveYear monthly repayment of 32.416666666666664', async () => {
        await expect(result.calculation.fiveYear.monthlyRepayment).toEqual(32.416666666666664);
    });

    test('fiveYear total repayment 1945', async () => {
        await expect(result.calculation.fiveYear.totalRepayment).toEqual(1945);
    });

    test('fiveYear total interest 945', async () => {
        await expect(result.calculation.fiveYear.totalInterest).toEqual(945);
    });

});





















