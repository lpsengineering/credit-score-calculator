const puppeteer = require('puppeteer');

const profile = {
    firstName: 'John',
    lastName: 'Doe',
    address: '123 Address',
    age: '20',
    gender: 'male',
    income: '20000',
    homeOwnership: 'own',
    loanAmount: '2000',
    type: 'personal',

};

describe('Submit Form', () => {
    test('Input client', async () => {
        let browser = await puppeteer.launch({
            headless: false
        });
        let page = await browser.newPage();

        page.emulate({
            viewport: {
            width: 1080,
            height: 1920
            },
            userAgent: ''
        });

        await page.goto('http://localhost:8080/dev/#/');
        await page.click("input[name=firstName]");
        await page.type("input[name=firstName]", profile.firstName);
        await page.click("input[name=lastName]");
        await page.type("input[name=lastName]", profile.lastName);
        await page.click("textarea[name=address]");
        await page.type("textarea[name=address]", profile.address);
        await page.click("input[name=age]");
        await page.type("input[name=age]", profile.age);
        await page.click("select[name=gender]");
        await page.select("select[name=gender]", profile.gender);
        await page.click("input[name=income]");
        await page.type("input[name=income]", profile.income);
        await page.click("select[name=homeOwnership]");
        await page.select("select[name=homeOwnership]", profile.homeOwnership);
        await page.click("input[name=loanAmount]");
        await page.type("input[name=loanAmount]", profile.loanAmount);
        await page.click("select[name=type]");
        await page.select("select[name=type]", profile.type);
        await page.click("button[type=submit]");

        const text = await page.evaluate(() => document.body.textContent);

        setTimeout(() => {
            expect(text).toContain('ACCEPTED');
        });
    });
});
