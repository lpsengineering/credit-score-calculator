const service = require('../../server/credit-score-service');

describe('Credit Score service Denial', () =>{
    let result = service.calculateCreditSummary({
        age: 20,
        homeOwnership: 'rent',
        income: 9999,
        loanAmount: 1000,
        type: 'personal'
    });

    test('Approval should be Denied', () =>{
        expect(result.approval).toMatch(/DENIED/);
    });

    test('Credit Score to equal 300', () =>{    
        expect(result.creditScore).toEqual(330);
    });

    test('Credit rating should be POOR', () => {
        expect(result.creditRating).toMatch(/Poor/);
    });
})

describe('Credit Score service Approval with Great credit rating', () =>{
    let result = service.calculateCreditSummary({
        age: 40,
        homeOwnership: 'own',
        income: 100000,
        loanAmount: 2000,
        type: 'personal'
    });

    test('Approval should be Accepted', () =>{
        expect(result.approval).toMatch(/ACCEPTED/);
    });

    test('Credit Score to equal 635', () =>{    
        expect(result.creditScore).toEqual(635);
    });

    test('Credit rating should be Great', () => {
        expect(result.creditRating).toMatch(/Great/);
    });

    test('Interest rate result of 6.99', () => {
        expect(result.interest).toEqual(6.99);
    });

    test('Loan Threshold of 70000', () => {
        expect(result.loanThreshold).toBeLessThanOrEqual(70000);
    });

    test('alculations exists', () => {
        expect(result.calculation).toHaveProperty('threeYear');
        expect(result.calculation).toHaveProperty('fiveYear');
    });

    test('threeYear have approved loan of 2000', () => {
        expect(result.calculation.threeYear.approvedLoan).toEqual(2000);
    });

    test('threeYear monthly repayment 67.20555555555556', () => {
        expect(result.calculation.threeYear.monthlyRepayment).toEqual(67.20555555555556);
    });

    test('threeYear total repayment 2419.4', () => {
        expect(result.calculation.threeYear.totalRepayment).toEqual(2419.4);
    });

    test('threeYear total interest 419.4', () => {
        expect(result.calculation.threeYear.totalInterest).toEqual(419.4);
    });

    test('fiveYear have approved loan of 2000', () => {
        expect(result.calculation.fiveYear.approvedLoan).toEqual(2000);
    });

    test('fiveYear monthly repayment 44.983333333333334', () => {
        expect(result.calculation.fiveYear.monthlyRepayment).toEqual(44.983333333333334);
    });

    test('fiveYear total repayment 2699', () => {
        expect(result.calculation.fiveYear.totalRepayment).toEqual(2699);
    });

    test('fiveYear total interest 699', () => {
        expect(result.calculation.fiveYear.totalInterest).toEqual(699);
    });
});

describe('Credit Score service Approval with Good credit rating', () =>{
    let result = service.calculateCreditSummary({
        age: 30,
        homeOwnership: 'own',
        income: 50000,
        loanAmount: 1000,
        type: 'business'
    });

    test('Approval should be Accepted', () =>{
        expect(result.approval).toMatch(/ACCEPTED/);
    });

    test('Credit Score to equal 545', () =>{    
        expect(result.creditScore).toEqual(545);
    });

    test('Credit rating should be Good', () => {
        expect(result.creditRating).toMatch(/Good/);
    });

    test('Interest rate result of 18.9', () => {
        expect(result.interest).toEqual(18.9);
    });

    test('Loan Threshold of 40000', () => {
        expect(result.loanThreshold).toBeLessThanOrEqual(40000);
    });

    test('alculations exists', () => {
        expect(result.calculation).toHaveProperty('threeYear');
        expect(result.calculation).toHaveProperty('fiveYear');
    });

    test('threeYear have approved loan of 1000', () => {
        expect(result.calculation.threeYear.approvedLoan).toEqual(1000);
    });

    test('threeYear monthly repayment 43.52777777777778', () => {
        expect(result.calculation.threeYear.monthlyRepayment).toEqual(43.52777777777778);
    });

    test('threeYear total repayment 1567', () => {
        expect(result.calculation.threeYear.totalRepayment).toEqual(1567);
    });

    test('threeYear total interest 567', () => {
        expect(result.calculation.threeYear.totalInterest).toEqual(567);
    });

    test('fiveYear have approved loan of 1000', () => {
        expect(result.calculation.fiveYear.approvedLoan).toEqual(1000);
    });

    test('fiveYear monthly repayment 32.416666666666664', () => {
        expect(result.calculation.fiveYear.monthlyRepayment).toEqual(32.416666666666664);
    });

    test('fiveYear total repayment 1945', () => {
        expect(result.calculation.fiveYear.totalRepayment).toEqual(1945);
    });

    test('fiveYear total interest 945', () => {
        expect(result.calculation.fiveYear.totalInterest).toEqual(945);
    });
});