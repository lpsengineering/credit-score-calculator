describe('Credit Score Calculator E2E Testing', function() {
  it('Should fill up all forms, proceed to next page and get approved', function() {
    browser.get('http://localhost:8080/dev/#/');

    expect(browser.getTitle()).toEqual('Credit Score Calculator');
    element(by.model('profile.firstName')).sendKeys("John");
    element(by.model('profile.lastName')).sendKeys("Smith");
    element(by.model('profile.address')).sendKeys("123 Address Street");
    element(by.model('profile.age')).sendKeys("25");
    element(by.model('profile.gender')).sendKeys("Male");
    element(by.model('profile.income')).sendKeys("50000");
    element(by.model('profile.homeOwnership')).sendKeys("own");
    element(by.model('profile.loanAmount')).sendKeys("5000");
    element(by.model('profile.type')).sendKeys("personal");
    var btnSubmit = element(by.buttonText('Submit'));

    btnSubmit.click().then(function() {
        var loanApproval = element(by.binding('loanApproval'));
        expect(loanApproval.getText()).toBe('ACCEPTED');
    });
  });


  it('Should fill up all forms, proceed to next page and get denied', function() {
    browser.get('http://localhost:8080/dev/#/');
    
    expect(browser.getTitle()).toEqual('Credit Score Calculator');
    element(by.model('profile.firstName')).sendKeys("John");
    element(by.model('profile.lastName')).sendKeys("Smith");
    element(by.model('profile.address')).sendKeys("123 Address Street");
    element(by.model('profile.age')).sendKeys("10");
    element(by.model('profile.gender')).sendKeys("Male");
    element(by.model('profile.income')).sendKeys("0");
    element(by.model('profile.homeOwnership')).sendKeys("rent");
    element(by.model('profile.loanAmount')).sendKeys("9999");
    element(by.model('profile.type')).sendKeys("personal");
    var btnSubmit = element(by.buttonText('Submit'));

    btnSubmit.click().then(function() {
        var loanApproval = element(by.binding('loanApproval'));
        expect(loanApproval.getText()).toBe('DENIED');
    });
  });

}); 